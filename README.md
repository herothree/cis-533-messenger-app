# README #

### Messenger App ###

This application implements the Ring Learning with Errors key exchange. It also uses AES encryption to allow two parties to communicate with each other using the shared key established during the RLWE exchange. It is currently comprised of roughly 1600 lines of code. A report of the methods and algorithms used here can be found in "report.pdf" in the top level of this repo. 

Jeremy Sigrist  
CIS 533 


### Source code ###

The most interesting parts of the source code are likely the key exchange methods in RLWE_Initiator.java and RLWE_Responder.java (found in the ui package).  

The Polynomial class took the largest amount of development time. It implements polynomial addition, scalar multiplication, polynomial multiplication, and polynomial long division. It is used heavily during the RLWE key exchange.  

The entry point for the application is Main.java in the encryption package.




### Who do I talk to? ###

jsigrist@cs.uoregon.edu