

package ui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;

import java.util.*;
import java.util.List;

/**
 * CIS 533 Messenger Window
 * 
 * Provides a user interface to send and receive messages
 * 
 * @author jeremy
 *
 */
public class MessengerWindow extends JFrame {
    /**
     * 
     */
    private static final long serialVersionUID = -4964365064260971422L;

    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;

    protected JTextArea display;
    protected JButton sendButton;
    protected JTextField currentMessage;
    protected Queue<String> inputQueue;
    protected Queue<String> outputQueue;
    protected List<Message> messages;
    protected Thread messageUpdater;
    protected DataInputStream dis;
    protected DataOutputStream dos;

    private byte[] key;

    public MessengerWindow(DataInputStream dis, DataOutputStream dos, byte[] key) {

        // TODO today: Finish this, read investopedia, start 533 report

        if (RIGHT_TO_LEFT) {
            this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
        
        setTitle("CIS 533 Messenger App");

        inputQueue = new ArrayDeque<String>();
        outputQueue = new ArrayDeque<String>();
        messages = new ArrayList<Message>();
        this.dis = dis;
        this.dos = dos;
        this.key = key;

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        if (shouldFill) {
            // natural height, maximum width
            c.fill = GridBagConstraints.HORIZONTAL;
        }

        // button = new JButton("Long-Named Button 4");
        JPanel middle = new JPanel();

        // Top left right bottom padding
        middle.setBorder(new EmptyBorder(10, 10, 10, 10));

        display = new JTextArea(16, 58);
        display.setEditable(false); // set textArea non-editable
        JScrollPane scroll = new JScrollPane(display);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        display.setText("hi");

        JPanel edge = new JPanel();
        edge.setBorder(new LineBorder(new Color(221, 237, 255), 10));
        edge.add(display);

        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 0; // make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridheight = 2;
        c.gridx = 0;
        c.gridy = 0;
        middle.add(edge);
        this.add(middle, c);

        sendButton = new JButton();
        sendButton.addActionListener(new SendActionListener());
        getRootPane().setDefaultButton(sendButton);
        

        sendButton.setText("Send");
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 0; // reset to default
        c.weighty = 1.0; // request any extra vertical space
        c.anchor = GridBagConstraints.PAGE_END; // bottom of space
        c.insets = new Insets(10, 0, 0, 0); // top padding
        c.gridx = 2; // aligned with button 2
        c.gridwidth = 1; // 2 columns wide
        c.gridy = 2; // third row
        this.add(sendButton, c);

        currentMessage = new JTextField();
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 0;
        c.ipadx = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor = GridBagConstraints.PAGE_END;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(currentMessage, c);

        messageUpdater = new Thread(new messageHandler());
        messageUpdater.start();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public JTextArea getDisplay() {
        return display;
    }

    public JButton getSend() {
        return sendButton;
    }

    public JTextField getCurrentMessage() {
        return currentMessage;
    }

    class messageHandler implements Runnable
    {
        public int PADDING = 20; 

        @Override
        public void run()
        {
            try
            {
                while (true)
                {
                    String in = inputQueue.poll();
                    if (null != in)
                    {
                        messages.add(new Message(in, true));
                    }

                    String out = outputQueue.poll();
                    if (null != out)
                    {
                        messages.add(new Message(out, false));

                        // Try to send the message
                        try
                        {
                            Messaging.sendMessage(out, dos, key);
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }

                    updateWindow();

                    // Check if there are any new messages to receive
                    try
                    {
                        if (dis.available() > 0)
                        {
                            //System.out.println("Receiving message");
                            inputQueue.add(Messaging.receiveMessage(dis, key));
                        }
                    } catch (IOException ex) { }

                    Thread.sleep(100);
                }

            }
            catch (InterruptedException ex)
            {

            }
        }

        public void updateWindow()
        {
            StringBuilder result = new StringBuilder();
            int width = (display.getSize().width/7 - PADDING);

            for (Message msg : messages)
            {
                if (msg.isReceived())
                {
                    for (int i = 0; i < (msg.getMsg().length() / width) + 1; ++i)
                    {
                        // Add spaces before hand
                        result.append(String.join("", Collections.nCopies(PADDING, " ")));
                        result.append(msg.getMsg().substring(i*width, 
                                Math.min(msg.getMsg().length(), i*width+width)));
                        result.append("\n");
                    }
                }
                else
                {
                    for (int i = 0; i < (msg.getMsg().length() / width) + 1; ++i)
                    {
                        result.append(msg.getMsg().substring(i*width, 
                                Math.min(msg.getMsg().length(), i*width+width)));
                        result.append("\n");
                    }
                }
            }
            display.setText(result.toString());
        }
    }
    
    class SendActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (!currentMessage.getText().trim().equals(""))
            {
                outputQueue.add(currentMessage.getText());
                currentMessage.setText("");
            }
        }
    }

    class Message
    {
        // The content of the message
        private String msg;

        // True if this message came from another client, false if we sent it
        private boolean received;


        public Message(String msg, boolean received)
        {
            super();
            this.msg = msg;
            this.received = received;
        }

        public String getMsg()
        {
            return msg;
        }

        public void setMsg(String msg)
        {
            this.msg = msg;
        }

        public boolean isReceived()
        {
            return received;
        }

        public void setReceived(boolean received)
        {
            this.received = received;
        }

        public String toString()
        {
            return String.format("[%b:%s]", isReceived(), getMsg());
        }
    }
}



