package ui;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import encryption.AES;

public class Messaging
{
    public static void sendMessage(String msg, DataOutputStream out, byte[] key) throws IOException
    {
        byte[] toSend = AES.encrypt(msg.getBytes(), key);
        out.writeInt(toSend.length);
        out.write(toSend);
    }
    
    public static String receiveMessage(DataInputStream in, byte[] key) throws IOException
    {
        int size = 0;
        byte[] fromLine = null;
        
        if ((size = in.readInt()) > 0)
        {
            fromLine = new byte[size];
            for (int i = 0; i < size; ++i)
            {
                fromLine[i] = in.readByte();
            }
        }
        
        return new String(AES.decrypt(fromLine, key));
    }
}
