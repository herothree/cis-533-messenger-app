package ui;

import java.io.*;
import java.net.*;
import java.util.*;
import encryption.*;

/**
 * One of the two parties in the RLWE key exchange (Party A in the project report). 
 * the keyExchange method is probably the most interesting part of this.
 */
public class RLE_Initiator implements Runnable {

    private byte[] key;
    
    public void keyExchange(ObjectInputStream objectInputStream, 
            ObjectOutputStream objectOutputStream) throws IOException, ClassNotFoundException
    {
        Polynomial s0 = Polynomial.genUniformSampled(512);
        Polynomial s1 = Polynomial.genUniformSampled(512);
        Polynomial a = new Polynomial("", new ArrayList<Integer>(RLEProtocol.round_list(new ArrayList<Double> (Polynomial.genUniformSampled(512).getCoefficients()))));
        Polynomial b = RLEProtocol.fix(RLEProtocol.fix(a.mul(RLEProtocol.fix(s1))).add(RLEProtocol.fix(s0)));
        
        objectOutputStream.writeObject(a);
        objectOutputStream.writeObject(b);
        
        Polynomial u = (Polynomial) objectInputStream.readObject();
        
        // Unfixable warnings are obnoxious 
        @SuppressWarnings("unchecked")
        List<Integer> vp = (List<Integer>) objectInputStream.readObject();
        
        // Party A:
        Polynomial w = RLEProtocol.fix(u.mul(s1));
        List<Integer> mup = RLEProtocol.rec(w, vp);
        key = RLEProtocol.makeKey(mup);        
    }
    

    
	@Override
	public void run()
	{
        String hostName = "localhost";
        int portNumber = RLEProtocol.PORT_NUMBER;
 
        try (
            Socket kkSocket = new Socket(hostName, portNumber);
            DataOutputStream out = new DataOutputStream(kkSocket.getOutputStream());
            DataInputStream in = new DataInputStream(kkSocket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(kkSocket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(kkSocket.getInputStream());) 
        {
            keyExchange(objectInputStream, objectOutputStream);
            
            MessengerWindow mw = new MessengerWindow(in, out, key);
            mw.setTitle("Initiator");
            
            while (mw.isVisible())
            {
                Thread.sleep(100);
            }
            
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }
        catch (ClassNotFoundException e)
        {
            System.err.println("Protocol error: unexpected object received from host.");
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
}