package ui;

import java.io.*;
import java.net.*;
import java.util.*;
import encryption.*;

/**
 * One of the two parties in the RLWE key exchange (party B in the project report)
 * the keyExchange method is probably the most interesting part of this.
 */
public class RLE_Responder implements Runnable {

    private byte[] key;
    
    int portNumber;
    
    private void keyExchange(ObjectInputStream objectInputStream, 
            ObjectOutputStream objectOutputStream) throws IOException, ClassNotFoundException
    {        
        Polynomial a = (Polynomial) objectInputStream.readObject();
        Polynomial b = (Polynomial) objectInputStream.readObject();
        
        Polynomial e0 = Polynomial.genUniformSampled(512);
        Polynomial e1 = Polynomial.genUniformSampled(512);
        Polynomial e2 = Polynomial.genUniformSampled(512);
        Polynomial u = RLEProtocol.fix(a.mul(e0).add(e1));
        Polynomial v = RLEProtocol.fix(b.mul(e0).add(e2));
        List<Integer> vr = RLEProtocol.round_list(RLEProtocol.randomized_round(v));
        List<Integer> vp = RLEProtocol.cross_round(vr);
        List<Integer> mu = RLEProtocol.modular_round(vr);
        
        objectOutputStream.writeObject(u);
        objectOutputStream.writeObject(vp);
        
        key = RLEProtocol.makeKey(mu);        
    }
    
	@Override
	public void run() {
        portNumber = RLEProtocol.PORT_NUMBER;
        
        try ( 
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            DataInputStream in = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
                ) {

            keyExchange(objectInputStream, objectOutputStream);
            
            MessengerWindow mw = new MessengerWindow(in, out, key);
            mw.setTitle("Responder");
            
            while (mw.isVisible())
            {
                Thread.sleep(100);
            }
            
        } catch (IOException e) {
            System.out.print("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection: ");
            System.out.println(e);
        }
        catch (ClassNotFoundException e)
        {
            System.err.println("Protocol error: unexpected object received from host");
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
	}
}