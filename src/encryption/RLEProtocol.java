
package encryption;

import java.util.*;

public class RLEProtocol
{
    public static final int PORT_NUMBER = 14052;

    // Higher numbers -> more info
    public static final int DEBUG = 1;

    // Algorithm parameters
    public static final int n = 512;
    public static final int m = 1024; // If you change this make sure to change the cyclotomic
                                      // polynomial as well. Wolfram Alpha can calculate cyclotomic(m).
    public static final int q = 25601;

    public static Random rand = new Random();

    public static Polynomial cyclotomic = new Polynomial(Arrays.asList(new Double[]{1.0,1.0}), Arrays.asList(new Integer[]{0, 512}));
    
    /**
     * Makes sure a polynomial is in the proper ring
     * @param toFix
     * @return
     */
    public static Polynomial fix(Polynomial toFix)
    {
        return new Polynomial(toFix).longDivide(RLEProtocol.cyclotomic).getRemainder().ringify();
    }
    
    public static byte[] makeKey(List<Integer> mu)
    {
        byte[] key = new byte[mu.size()];
        
        for (int i = 0; i < mu.size(); ++i)
        {
            if (mu.get(i) == 1)
            {
                key[i] = 1;
            }
            else if (mu.get(i) == 0)
            {
                key[i] = 0;
            }
            else
            {
                throw new IllegalArgumentException("The key list "
                        + "should be comprised of only zeros and ones");
            }
        }
        
        return key;
    }
    
    public static List<Integer> round_list(List<Double> vec)
    {
        ArrayList<Integer> result = new ArrayList<Integer>(vec.size());
        for (Double d : vec)
        {
            result.add(d.intValue());
        }

        return result;
    }
    
    // #############################################
    // # Rounding functions
    // #############################################
    // a = Rq.random_element()
    // def randomized_round(a): # Input an element of Rq
    public static List<Double> randomized_round(Polynomial coeff)
    {
        // """
        // Randomized rounding for creating the bitstring to send inside the
        // protocol (simplification eliminating the need to map into 2q)
        // """
        // coeff = Rq(a).list();
        // round_coeff = [];
        List<Double> round_coeff = new ArrayList<Double>();

        // for i in range(n):
        for (int i = 0; i < n; i++)
        {
            // if coeff[i] == 0:
            if (coeff.get(i) == 0)
            {
                // tmp=randrange(0, 2)
                int tmp = rand.nextInt(2);
                // if tmp == 0:
                if (tmp == 0)
                {
                    // round_coeff.append(1)
                    round_coeff.add(1.0);
                }
                // else:
                else
                {
                    // round_coeff.append(q-1)
                    round_coeff.add(q - 1.0);
                }
            }
            // elif coeff[i] == (q-1)/4:
            else if (coeff.get(i) == (q - 1) / 4)
            {
                // tmp=randrange(0,2)
                int tmp = rand.nextInt(2);
                // if tmp==0:
                if (tmp == 0)
                {
                    // round_coeff.append(coeff[i])
                    round_coeff.add(coeff.get(i));
                    // }
                }
                else
                {
                    // round_coeff.append(coeff[i]+1)
                    round_coeff.add(coeff.get(i) + 1);
                }
            }
            // else:
            else
            {
                // round_coeff.append(coeff[i])
                round_coeff.add(coeff.get(i));
            }

        }
        // return round_coeff
        return round_coeff;

    }
    
    // def modular_round(a):
    // """ Input randomized rounded a in Rq as a vector of coefficients. """
    // round_coeff = []
    // for i in range(n):
    // return round_coeff
    public static List<Integer> modular_round(List<Integer> a)
    {
        List<Integer> round_coeff = new ArrayList<Integer>(n);
        for (int i = 0; i < n; ++i)
        {
            // round_coeff.append(     round(2 * (a[i].lift() -      round(q/4  ))/        q) %2);
            round_coeff.      add(Math.round(2 * (a.get(i)    - Math.round(q/4.0))/((float)q))%2);
        }
        
        return round_coeff;
    }
    
    public static List<Integer> cross_round(List<Integer> a)
    {
        List<Integer> round_coeff = new ArrayList<Integer>(n);
        for (int i = 0; i < n; ++i)
        {
            // round_coeff.append(floor(4*a[i].lift()/q)%2)
            round_coeff.add((int) (Math.floor((4.0 * a.get(i)) / q)%2));
        }
        return round_coeff;
    }
            
    public static List<Integer> rec(Polynomial a, List<Integer> b)
    {
        List<Integer> key = new ArrayList<Integer>();
        for (int i = 0; i < n; ++i)
        {
            if (b.get(i) == 0)
            {
                if ((a.get(i) >= 0 && a.get(i) < Math.floor(q/4.0 + q/8.0)+1) 
                        || (a.get(i) >= q - 1 - Math.floor(q/8.0) && a.get(i) < q))
                {
                    key.add(0);
                }
                else
                {
                    key.add(1);
                }
            }
            
            if (b.get(i) == 1)
            {
                if ((a.get(i) >= Math.floor(q/4.0-q/8.0) && a.get(i) < (q-1)/2.0+Math.floor(q/8.0)+1))
                {
                    key.add(0);
                }
                else
                {
                    key.add(1);
                }
            }
        }
        return key;
    }


}
