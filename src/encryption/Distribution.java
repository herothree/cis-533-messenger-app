package encryption;

public interface Distribution
{
    public int next();
}
