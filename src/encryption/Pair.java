package encryption;

public class Pair
{
    protected Polynomial quotient;
    protected Polynomial remainder;
    
    
    
    public Pair(Polynomial quotient, Polynomial remainder)
    {
        super();
        this.quotient = quotient;
        this.remainder = remainder;
    }
    
    public Pair()
    {
        this(new Polynomial(), new Polynomial());
    }

    public Polynomial getQuotient()
    {
        return quotient;
    }
    public void setQuotient(Polynomial quotient)
    {
        this.quotient = quotient;
    }
    public Polynomial getRemainder()
    {
        return remainder;
    }
    public void setRemainder(Polynomial remainder)
    {
        this.remainder = remainder;
    }
    
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        result.append("quotient: ");
        result.append(quotient);
        result.append(". remainder: ");
        result.append(remainder);
        return result.toString();
    }
}
