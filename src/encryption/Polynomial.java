package encryption;

import java.io.Serializable;
import java.util.*;

public class Polynomial implements Serializable
{    
    private static final long serialVersionUID = -5984364723236747613L;

    private LinkedList<Double> coefficients;
    
    protected static Distribution dist;

    public Polynomial(List<Double> coefficients, List<Integer> powers)
    {
        this();
        assert coefficients.size() == powers.size();
        
        for (int i = 0; i < coefficients.size(); ++i)
        {
            this.set(powers.get(i), coefficients.get(i));
        }
    }
    
    /**
     * @param coefficients List of the coefficients of each power of x, in ascending order
     * so the coefficient of x_0 comes first. 
     */
    public Polynomial(List<Double> coefficients)
    {
        this.setCoefficients(new LinkedList<Double>(coefficients));
        
        // Initialize this lazily
        dist = null;
    }
    
    public Polynomial(String isInt, List<Integer> coef)
    {
        this();
        
        // This method signature is a hack to let me initialize the Polynomial with
        // a list of integers
        for (Integer i : coef)
        {
            getCoefficients().add(new Double(i));
        }
        
        
    }
    
    public Polynomial(Polynomial D)
    {
        this(D.getCoefficients());
    }

    public Polynomial()
    {
        this(new ArrayList<Double>());
    }
    
    public Double get(int index)
    {
        if (index < this.getCoefficients().size())
        {
            return this.getCoefficients().get(index);
        }
        else
        {
            return 0.0;
        }
    }
    
    public void set(int index, Double value)
    {
        // Sets the coefficient of x ^ index
        while (index >= getCoefficients().size())
        {
            getCoefficients().add(0.0);
        }
        getCoefficients().set(index, value);
    }
    
    public int degree()
    {
        // No need to store zeros
        
        while (getCoefficients().size() > 0 && 0 == getCoefficients().get(getCoefficients().size()-1))
        {
            getCoefficients().remove(getCoefficients().size()-1);
        }
        
        
        return getCoefficients().size()-1;
    }

    public Polynomial add(Polynomial other)
    {
        Polynomial result = new Polynomial();
        
        // Iterate through the linked lists in parallel
        Iterator<Double> t = this.getCoefficients().iterator();
        Iterator<Double> o = other.getCoefficients().iterator();
        
        // Use this method to efficiently iterate through the linked lists
        while (t.hasNext() && o.hasNext())
        {
            result.getCoefficients().add(t.next() + o.next());
        }
        
        // At least one of these loops will not run because the iterator has run out
        while (t.hasNext())
        {
            result.getCoefficients().add(t.next());
        }
        while (o.hasNext())
        {
            result.getCoefficients().add(o.next());
        }
        
        return result;
    }
    
    public Polynomial sub(Polynomial other)
    {
        // Subtract other from this.
        return this.add(other.scalerMul(-1));
    }
    
    public Polynomial scalerMul(Integer scalar)
    {
        Polynomial result = new Polynomial();
        
        for (Double c : this.getCoefficients())
        {
            result.getCoefficients().add(scalar * c);
        }
        
        return result;
    }
    
    public Polynomial mul(Polynomial other)
    {
        ArrayList<Double> result = new ArrayList<Double>();
        Double s;
        
        ArrayList<Double> otherV = new ArrayList<Double>(other.getCoefficients());
        ArrayList<Double> thisV = new ArrayList<Double>(this.getCoefficients());
        
        // s_i = p_0q_i + p_1q_(i-1) + ... + p_iq_0
        
        int nsize = this.degree() + other.degree() + 1;
        
        for (int i = 0; i < nsize; ++i)
        {
            s = 0.0;
            for (int j = 0; j <= i; j++)
            {
                if (j < thisV.size() && (i-j) < otherV.size())
                {
                    s = (s + thisV.get(j) * otherV.get(i-j));
                    
                    // Turns out this is faster than using mod every time.
                    if (Math.abs(s) > RLEProtocol.q)
                    {
                        s = s % RLEProtocol.q;
                        if (s < 0)
                        {
                            s += RLEProtocol.q;
                        }
                    }
                }
            }
            result.add(s);
        }
        
        return new Polynomial(result);
    }

    public Pair longDivide(Polynomial D)
    {
        Pair result = new Pair();
        
        Polynomial q;
        Polynomial r;

        if (D.degree() < 0)
        {
            throw new ArithmeticException("Divide by zero polynomial error");
        }

        if (this.degree() >= D.degree())
        {

            q = new Polynomial();
            r = new Polynomial(this);

            while (r.degree() >= 0 && r.degree() >= D.degree())
            {
                Polynomial tmp = new Polynomial();
                Double t = (r.get(r.degree())) / D.get(D.degree());
                
                
                
                // Append t to q
                tmp.set(r.degree() - D.degree(), new Double(t));
                q = q.add(tmp);
                
                
                r = r.sub(D.mul(tmp));

            }
            
            result.setQuotient(q);
            result.setRemainder(r);
        }

        else
        {
            result.setQuotient(new Polynomial());
            result.setRemainder(new Polynomial(this));
        }
        
        return result;
    }
    
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        ArrayList<Double> contents = new ArrayList<Double>(this.getCoefficients());
        
        if (getCoefficients().size() == 0)
        {
            return "0";
        }
        
        // Iterate through the linked list for efficiency
        for (int i = contents.size()-1; i >= 0; --i)
        {
            // Make sure not to put a plus before the first number
            if (contents.size() != i+1)
            {
                result.append(" + ");
            }
            result.append(contents.get(i));
            
            if (i > 1)
            {
                result.append("*zz^");
                result.append(i);
            }
            else if (i == 1)
            {
                result.append("*zz");
            }
            
        }

        return result.toString();
    }

    public static Polynomial genUniformSampled(int length)
    {
        Polynomial result = new Polynomial();
        
        if (null == dist)
        {
            // TODO: is this right?
            // Default to q = 0 and b = 5
            dist = new UniformDistribution(0, 5);
        }
        
        for (int i = 0; i < length; ++i)
        {
            result.getCoefficients().add(new Double(dist.next()));
        }
        
        return result;
    }
    
    public static void setDistribution(Distribution d)
    {
        dist = d;
    }
    
    public List<Integer> formattedCoefficients()
    {
        List<Integer> result = new ArrayList<Integer>();
        
        for (Double d : this.getCoefficients())
        {
            result.add(d.intValue());
        }
        
        return result;
    }
    
    public Polynomial ringify()
    {
        // Make sure every number is between 0 and RLEProtocol.q
        Polynomial result = new Polynomial();
        for (Double d : this.getCoefficients())
        {
            
            // Make sure negative numbers wrap around to positive
            if (d >= 0)
            {
                result.getCoefficients().add((double) ((d.intValue() % RLEProtocol.q)));                
            }
            else
            {
                // Note that -11 % 5 = -1.
                result.getCoefficients().add((double) (d.intValue()%RLEProtocol.q)+RLEProtocol.q);                
            }
        }
        return result;
    }
    
    public static void main(String[] args)
    {
        Polynomial poly1 = new Polynomial(
                Arrays.asList(new Double[] { -42.0, 0.0, -12.0, 3.0}));
        Polynomial poly2 = new Polynomial(
                Arrays.asList(new Double[] {1.0, 2.0}));
        
        //Pair p = poly1.longDivide(poly2);
        //System.out.println("q: " + p.getQuotient());
        //System.out.println("r: " + p.getRemainder());
        
        
        // 1 + x + x2
        // 1 + x2 + 3x3

        System.out.println(poly1);
        System.out.println(poly2);
        System.out.println("poly1 + poly2: " + poly2.add(poly1));
        /*
        System.out.println("-: " + poly1.sub(poly2));
        System.out.println("3*: " + poly2.scalerMul(3));
        System.out.println("*: " + poly2.mul(poly1));
        */
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((getCoefficients() == null) ? 0 : getCoefficients().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Polynomial other = (Polynomial) obj;
        if (getCoefficients() == null)
        {
            if (other.getCoefficients() != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        // Degree will trim the leading zeros off of the internal polynomials
        return ((this.degree() == other.degree()) && getCoefficients().equals(other.getCoefficients()));
    }

    /**
     * @return the coefficients
     */
    public LinkedList<Double> getCoefficients()
    {
        return coefficients;
    }

    /**
     * @param coefficients the coefficients to set
     */
    public void setCoefficients(LinkedList<Double> coefficients)
    {
        this.coefficients = coefficients;
    }

}
