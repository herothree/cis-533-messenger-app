package encryption;

import java.util.*;

import ui.RLE_Initiator;
import ui.RLE_Responder;

public class Main
{

    /**
     * This method runs both sides of the key exchange and prints out the values used.
     * @throws InterruptedException
     */
    public static void demo() throws InterruptedException
    {
        int diff = 0;

        // Initialization
        Polynomial s0 = Polynomial.genUniformSampled(512);
        Polynomial s1 = Polynomial.genUniformSampled(512);

        Polynomial e0 = Polynomial.genUniformSampled(512);
        Polynomial e1 = Polynomial.genUniformSampled(512);
        Polynomial e2 = Polynomial.genUniformSampled(512);

        Polynomial a = new Polynomial("", new ArrayList<Integer>(RLEProtocol.round_list(new ArrayList<Double> (Polynomial.genUniformSampled(512).getCoefficients()))));
        System.out.println("a: " + a);

        long starttime = System.nanoTime();
        Polynomial b = RLEProtocol.fix(RLEProtocol.fix(a.mul(RLEProtocol.fix(s1))).add(RLEProtocol.fix(s0)));
        System.out.println("b: " + b);

        Polynomial u = RLEProtocol.fix(a.mul(e0).add(e1));
        Polynomial v = RLEProtocol.fix(b.mul(e0).add(e2));

        System.out.println("u: " + u);
        System.out.println("v: " + v);

        List<Integer> vr = RLEProtocol.round_list(RLEProtocol.randomized_round(v));
        System.out.println("vr: " + vr);

        List<Integer> vp = RLEProtocol.cross_round(vr);
        System.out.println("vp: " + vp);

        Polynomial w = RLEProtocol.fix(u.mul(s1));
        System.out.println("w: " + w);

        List<Integer> mu = RLEProtocol.modular_round(vr);
        System.out.println("mu:  " + mu);

        List<Integer> mup = RLEProtocol.rec(w, vp);
        System.out.println("mup: " + mup);

        System.out.println(mu.equals(mup));

        diff = 0;
        for (int ii = 0; ii < mu.size(); ++ii)
        {
            if (mup.get(ii) != mu.get(ii))
            {
                diff++;
            }
        }
        System.out.println("diff: " + diff);

        long endtime = System.nanoTime();

        System.out.printf("Time taken: %d milliseconds\n", (endtime - starttime) / 1000000L);

        if (diff >= 1)
        {
            System.out.println("\nCopyable values:");
            System.out.println("s0: " + s0.formattedCoefficients());
            System.out.println("s1: " + s1.formattedCoefficients());
            System.out.println("e0: " + e0.formattedCoefficients());
            System.out.println("e1: " + e1.formattedCoefficients());
            System.out.println("e2: " + e2.formattedCoefficients());
            System.out.println("a: " + a.formattedCoefficients());
            System.out.println("b: " + b.formattedCoefficients());
            System.out.println("u: " + u.formattedCoefficients());
            System.out.println("v: " + v.formattedCoefficients());
            System.out.println("vr: " + vr);
            System.out.println("vp: " + vp);
            System.out.println("w: " + w.formattedCoefficients());
            System.out.println("mu:  " + mu);
            System.out.println("mup: " + mup);
        }
    }
    
    /**
     * Launches two threads, each of which communicates with the other via sockets
     * to establish a shared secret key and then send encrypted messages.
     * @param args Not used
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException
    {
        RLE_Responder sender = new RLE_Responder();
        RLE_Initiator receiver = new RLE_Initiator();

        Thread senderThread = new Thread(sender);
        Thread receiverThread = new Thread(receiver);
        
        receiverThread.start();
        senderThread.start();

        senderThread.join(10000);
        receiverThread.join(10000);
    }

}
