package encryption;

import java.util.*;

public class UniformDistribution implements Distribution
{
    public Random rand;
    public int q;
    public int b;
    
    public UniformDistribution(int q, int b)
    {
        rand = new Random();
        this.q = q;
        this.b = b;
    }
    
    public int next()
    {
        // https://en.wikipedia.org/wiki/Ring_learning_with_errors_key_exchange#Introduction
        
        return (q-b) + rand.nextInt(2 * b + 1);
    }

}
